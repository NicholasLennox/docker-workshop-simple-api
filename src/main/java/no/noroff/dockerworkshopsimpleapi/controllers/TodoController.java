package no.noroff.dockerworkshopsimpleapi.controllers;

import no.noroff.dockerworkshopsimpleapi.models.Todo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class TodoController {
    @GetMapping("todos")
    public ResponseEntity<List<Todo>> getAll() {
        List<Todo> todos = Arrays.asList(
                new Todo(1, "Make breakfast"),
                new Todo(2, "Wash dishes"),
                new Todo(3, "Walk dog")
        );
        return ResponseEntity.ok(todos);
    }
}
