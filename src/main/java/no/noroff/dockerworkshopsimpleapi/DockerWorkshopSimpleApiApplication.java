package no.noroff.dockerworkshopsimpleapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerWorkshopSimpleApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DockerWorkshopSimpleApiApplication.class, args);
    }

}
